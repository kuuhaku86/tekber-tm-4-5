package com.example.tmempatlima;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AnotherActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText searchUrlEditText, locationEditText, shareThisTextEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        searchUrlEditText = findViewById(R.id.search_url_edit_text);
        locationEditText = findViewById(R.id.location_edit_text);
        shareThisTextEditText = findViewById(R.id.share_this_text_edit_text);

        Integer[] idsButton = {
               R.id.search_url_button,
               R.id.location_button,
               R.id.share_this_text_button,
               R.id.next_activity
        };

        for (int i = 0; i < idsButton.length; i++) {
            Button button = findViewById(idsButton[i]);
            button.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        Uri uri;
        Intent intent;
        switch (v.getId()) {
            case R.id.search_url_button:
                uri = Uri.parse(searchUrlEditText.getText().toString());
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                break;
            case R.id.location_button:
                uri = Uri.parse("geo: " + locationEditText.getText().toString());
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

                break;
            case R.id.share_this_text_button:
                String text = shareThisTextEditText.getText().toString();
                String mimeType = "text/plain";

                ShareCompat.IntentBuilder
                        .from(this)
                        .setType(mimeType)
                        .setChooserTitle(R.string.share_this_text)
                        .setText(text)
                        .startChooser();
                break;
            case R.id.next_activity:
                intent = new Intent(this, InputControlsActivity.class);
                startActivity(intent);

                break;
        }
    }
}