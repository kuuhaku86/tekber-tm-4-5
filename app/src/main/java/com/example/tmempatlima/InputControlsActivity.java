package com.example.tmempatlima;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class InputControlsActivity extends AppCompatActivity {
    private TextView seekBarText, switchText;
    private SeekBar seekBar;
    private Switch aSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_controls);

        seekBar = findViewById(R.id.seekBar);
        aSwitch = findViewById(R.id.switch2);
        seekBarText = findViewById(R.id.seekBarText);
        switchText = findViewById(R.id.switchText);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarText.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    switchText.setText("On");
                } else {
                    switchText.setText("Off");
                }
            }
        });
    }

    public void onImageClicked(View view) {
        Toast toast = new Toast(getApplicationContext());
        toast.setText("This is Image View");
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}